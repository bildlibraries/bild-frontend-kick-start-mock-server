/**
 * Created by Lindon Camaj on 5/6/2016.
 */

// ================================== requirements =====================================

var express = require("express");
var bodyParser = require('body-parser');
var bild = require('./src/app');

var app = express();
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));
// parse application/json
app.use(bodyParser.json());


bild.Core.Config.init(app);
bild.Routes.registerRoutes(app);

// Start server
app.listen(6634, function () {
    console.log("Server is up");
});
